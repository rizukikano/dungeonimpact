using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterCard : MonoBehaviour
{
    public CharacterData characterCard;
    [SerializeField] private Image img;
    public Image rarityImg;
    public GameObject lockGO;
    [SerializeField] private TextMeshProUGUI name,HP,Att,Def;
    public void updateCard(){
        img.sprite = characterCard.image;
        name.text = characterCard.characterName;
        HP.text = characterCard.health.ToString();
        Att.text = characterCard.attack.ToString();
        Def.text = characterCard.defense.ToString();
        rarityImg.color = characterCard.SetRarityColor();
    }
}
