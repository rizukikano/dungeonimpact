using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum RarityType
{
    Common,
    Rare,
    Epic
}
[System.Serializable]
public class GachaRate 
{
    public string rarity;

    public RarityType rarityType;

    [Range(1,100)]
    public int rate;
}
