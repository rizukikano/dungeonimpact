using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int poolSize = 10;

    private List<GameObject> pooledEnemies;

    void Start()
    {
        InitializePool();
        
    }

    void InitializePool()
    {
        pooledEnemies = new List<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
            GameObject enemy = Instantiate(enemyPrefab);
            enemy.SetActive(false);
            pooledEnemies.Add(enemy);
        }
    }

    public IEnumerator SpawnEnemies()
    {
        while (true)
        {
            // Get an inactive enemy from the pool
            GameObject enemy = GetPooledEnemy();

            if (enemy != null)
            {
                enemy.transform.position = GetRandomSpawnPosition();
                enemy.GetComponent<Enemy>().healthPoints = 25;
                enemy.SetActive(true);
            }

            yield return new WaitForSeconds(2.0f);
        }
    }

    GameObject GetPooledEnemy()
    {
        return pooledEnemies.Find(enemy => !enemy.activeInHierarchy);
    }

    Vector3 GetRandomSpawnPosition()
    {
        return new Vector3(Random.Range(-10f, 10f), 0f, Random.Range(-10f, 10f));
    }
}
