using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public string characterName;
    public int attack;
    public int healthPoints;
    public int defense;
    public CharacterData characterData;
    public WeaponData equipedWeapon;
    protected bool isAlive = true;

    // Method to perform an attack on another character
    public virtual void Attack(Character target)
    {
        int damageDealt = Mathf.Max(0, attack - target.defense);
        target.TakeDamage(damageDealt);
    }
    protected void MoveTowards(Transform target, float moveSpeed)
    {
        Vector2 direction = target.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(Vector3.forward * angle);
    }
    public bool IsDead()
    {
        return !isAlive;
    }
    public void UpdateCard(){
        characterName = characterData.characterName;
        healthPoints = characterData.health;
        if(equipedWeapon != null){
        attack = characterData.attack + equipedWeapon.baseAttack;
        }else{
            attack = characterData.attack;
        }
        defense = characterData.defense;
    }

    // Method to take damage
    public void TakeDamage(int damage)
    {
        healthPoints = Mathf.Max(0, healthPoints - damage);
        if (healthPoints == 0)
        {
            Die();
        }
    }

    // Method to handle character death
    protected virtual void Die()
    {
    }
    public static Character GetClosestCharacter(Transform currentTransform, List<Character> characters)
    {
        characters.RemoveAll(character => character == null || (character.gameObject != null && !character.gameObject.activeSelf));

        Character closestCharacter = null;
        float closestDistance = float.MaxValue;

        foreach (Character character in characters)
        {
            float distance = Vector3.Distance(currentTransform.position, character.transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestCharacter = character;
            }
        }

        return closestCharacter;
    }
}