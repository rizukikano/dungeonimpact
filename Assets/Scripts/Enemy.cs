using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Enemy : Character
{
    public float detectionRange = 10f;
    public float attackRange = 2f;
    public float moveSpeed = 5f;
    public float attackCooldown = 3f; // Cooldown in seconds
    private bool canAttack = true;
    public int scoreValue ;

    private List<Character> heroes = new List<Character>();
    private Character targetHero;

    void Start()
    {
        isAlive = true;
        // Find all GameObjects with the "Hero" tag and add their Character components to the list
        GameObject[] heroObjects = GameObject.FindGameObjectsWithTag("Hero");
        foreach (GameObject heroObject in heroObjects)
        {
            Character hero = heroObject.GetComponent<Character>();
            if (hero != null)
            {
                heroes.Add(hero);
            }
        }

        if (heroes.Count == 0)
        {
            Debug.LogError("No heroes found! Make sure heroes have the 'Hero' tag and inherit from the Character class.");
        }
    }

    void Update()
    {
        // Choose the closest hero within detection range
        if (targetHero == null || targetHero.IsDead())
        {
            targetHero = GetClosestHero();
        }

        if (targetHero != null)
        {
            float distanceToTarget = Vector3.Distance(transform.position, targetHero.transform.position);

            if (distanceToTarget > attackRange && canAttack)
            {
                // Move towards the target
                MoveTowards(targetHero.transform, moveSpeed);
            }
            else if (canAttack)
            {
                // Attack the target and start cooldown
                Attack(targetHero);
                StartCoroutine(AttackCooldown());
            }
        }
        
    }
    private IEnumerator AttackCooldown()
    {
        canAttack = false;
        yield return new WaitForSeconds(attack);
        canAttack = true;
    }


    // Override the Die method for the Enemy class
    protected override void Die()
    {
        //base.Die(); // Call the base class's Die method
        isAlive = false;
        GameManager.Instance.IncreaseScore(scoreValue);
        gameObject.SetActive(false);


        // Additional logic specific to the Enemy's death can be added here.
    }

    private Character GetClosestHero(){
        return GetClosestCharacter(transform,heroes);
    }


}
