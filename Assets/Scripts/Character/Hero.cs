using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Character
{
    public float attackRange = 2f;
    public float moveSpeed = 5f;
    public float attackCooldown = 1f;
    // Override the Attack method for the Fighter class
    private List<Character> enemies = new List<Character>();
    private bool canAttack = true;
    private Character targetEnemy;

    void Start()
    {
        // Find existing enemies in the scene and add them to the list
        
    }
    protected override void Die()
    {
        //base.Die(); // Call the base class's Die method
        Destroy(this.gameObject);
        // Additional logic specific to the Enemy's death can be added here.
    }

    void Update()
    {
        FindExistingEnemies();
        // Choose the closest enemy within attack range
        if (targetEnemy == null || targetEnemy.IsDead()|| !targetEnemy.gameObject.activeSelf)
        {
            targetEnemy = GetClosestEnemy();
        }
        

        if (targetEnemy != null && targetEnemy.gameObject.activeSelf)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, targetEnemy.transform.position);

            if (distanceToEnemy > attackRange && canAttack)
            {
                // Move towards the target
                MoveTowards(targetEnemy.transform, moveSpeed);
            }
            else if (canAttack)
            {
                // Attack the target and start cooldown
                Attack(targetEnemy);
                StartCoroutine(AttackCooldown());
            }
        }
    }

    void FindExistingEnemies()
    {
        // Find all GameObjects with the "Enemy" tag and add their Character components to the list
        GameObject[] enemyObjects = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemyObject in enemyObjects)
        {
            Character enemy = enemyObject.GetComponent<Character>();
            if (enemy != null && !enemies.Contains(enemy))
            {
                enemies.Add(enemy);
            }
        }
    }

    // Cooldown coroutine
    private IEnumerator AttackCooldown()
    {
        canAttack = false;
        yield return new WaitForSeconds(attackCooldown);
        canAttack = true;
    }

    private Character GetClosestEnemy()
    {
        return GetClosestCharacter(transform, enemies);
    }

    // Call this method when a new enemy is instantiated

}
