using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public List<Character> allCharacter;
    public List<Character> activeCharacter;
    public List<CharacterData> activeCharacterData;
    public List<WeaponData> activeWeaponData;
    public GameObject characterGO;
    public Transform pos,parent;
    public Text scoreText;
    public Text gameOverText;
    public int score = 0;
    public EnemyPool enemyPool;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        // InstantiateCharacter();
        score = 0;
        // UpdateScoreText();

        //gameOverText.gameObject.SetActive(false);
    }
    public void InstantiateCharacter(){
        activeCharacterData.Clear();
        foreach (CharacterCard charCard in CharacterManager.Instance.activeCharCard)
        {
            activeCharacterData.Add(charCard.characterCard);
        }
    }
    public void InstantiateWeapon(){
        activeWeaponData.Clear();
        foreach (WeaponCard weaponCard in CharacterManager.Instance.activeWeaponCards)
        {
            activeWeaponData.Add(weaponCard.weaponCard);
        }
    }
    public void SetActiveCharacterFromData()
    {
        activeCharacter.Clear(); // Clear the existing activeCharacter list

        foreach (CharacterData characterData in activeCharacterData)
        {
            // Find the corresponding Character object from allCharacter based on the ID or some other identifier
            Character character = allCharacter.Find(c => c.characterData.id == characterData.id);

            // If a matching Character is found, add it to the activeCharacter list
            if (character != null)
            {
                activeCharacter.Add(character);
            }
        }
    }
    public void SetWeaponToCharacter(){
        for(int i = 0; i<activeCharacter.Count;i++){
            if(activeWeaponData[i] != null){
                activeCharacter[i].equipedWeapon = activeWeaponData[i];
                activeCharacter[i].UpdateCard();
            }
        }
    }
    public void SpawnCharacter(){
        foreach(Character character in activeCharacter){
            GameObject characterGameObject = Instantiate(characterGO,pos.position,Quaternion.identity) as GameObject;
            characterGameObject.transform.SetParent(parent);
            characterGameObject.transform.localScale = Vector3.one;
            characterGameObject.GetComponent<SpriteRenderer>().color = character.characterData.SetRarityColor();
            Character tempchar = characterGameObject.GetComponent<Character>();
            if(character.equipedWeapon !=null){
                tempchar.equipedWeapon = character.equipedWeapon;
            }
            tempchar.characterData = character.characterData;
            tempchar.UpdateCard();

        }
    }
    public void SpawnEnemies(){
        StartCoroutine(enemyPool.SpawnEnemies());
    }

    public void IncreaseScore(int amount)
    {
        score += amount;
        // UpdateScoreText();
    }

    public void GameOver()
    {
        // Display game over text or perform other game over actions
        gameOverText.gameObject.SetActive(true);
    }
    public void Exit(){
        Application.Quit();
    }

    // void UpdateScoreText()
    // {
    //     if (scoreText != null)
    //     {
    //         scoreText.text = "Score: " + score;
    //     }
    // }
}
