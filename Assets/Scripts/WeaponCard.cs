using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeaponCard : MonoBehaviour
{
    public WeaponData weaponCard;
    [SerializeField] private Image img;
    public Image rarityImg;
    [SerializeField] private TextMeshProUGUI name,baseAtt,spEff;
    // Update is called once per frame
    public void updateCard(){
        img.sprite =weaponCard.image;
        name.text = weaponCard.weaponName;
        baseAtt.text = weaponCard.baseAttack.ToString();
        spEff.text = weaponCard.specialEffect;
        rarityImg.color = weaponCard.SetRarityColor();
    }
    
}
