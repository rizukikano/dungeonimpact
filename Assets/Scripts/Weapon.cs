using UnityEngine;

public class Weapon : MonoBehaviour
{
    public WeaponData weaponData;

    void Start()
    {
        Debug.Log($"{weaponData.weaponName} created with Rarity: {weaponData.rarity}, Base Attack: {weaponData.baseAttack}, Special Effect: {weaponData.specialEffect}");
    }
}