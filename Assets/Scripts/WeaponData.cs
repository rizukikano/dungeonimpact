using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "Gacha/Weapon Data")]
public class WeaponData : ScriptableObject
{
    public int id;
    [Header("UI")]
    public Sprite image;
    public string weaponName;
    public RarityType rarity;
    [Header("Stats")]
    public int baseAttack;
    public string specialEffect;
    public Color SetRarityColor()
    {
        switch (rarity)
        {
            case RarityType.Common:
                return Color.green;
            case RarityType.Rare:
                return Color.blue;
            case RarityType.Epic:
                return Color.magenta;
            default:
                // Handle other cases or leave it blank for a default color
                return Color.white;

        }
    }
}