using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInventoryManager : MonoBehaviour
{
    public static WeaponInventoryManager Instance;
    public List<WeaponCard> weaponCards = new List<WeaponCard>();

    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    public void Add(WeaponCard weaponCard){
        weaponCards.Add(weaponCard);
    }
}
