using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GachaManager : MonoBehaviour
{
    [SerializeField] private GachaRate[] gacha;
    [SerializeField] private Transform parent,pos,history;
    [SerializeField] private GameObject characterCardGO;
    [SerializeField] private GameObject weaponCardGO;
    public CharacterData[] characterReward;
    public WeaponData[] weaponReward;
    CharacterCard characterCard;
    WeaponCard weaponCard;
    WeaponCard[] weaponCards = new WeaponCard[100];
    [SerializeField] private SerializableList<int> characterCardsGachaed ;
    [SerializeField] private SerializableList<int> weaponCardsGachaed ;
    [SerializeField] private string characterDataPath;
    [SerializeField] private string weaponDataPath;
    
    void Start()
    {
        characterDataPath = Path.Combine(Application.persistentDataPath, "characterData.json");
        weaponDataPath = Path.Combine(Application.persistentDataPath, "weaponData.json");
        LoadWeaponDataFromJson();
        InventoryWeapon();
    }
    public void Gacha(){

        int rndType = UnityEngine.Random.Range(1,3);
        int rnd = UnityEngine.Random.Range(1,101);
        if(rndType == 1)
        {
            characterCard = InstatiateCard(characterCardGO,parent).GetComponent<CharacterCard>();

            for(int i=0; i<gacha.Length; i++){
                if(rnd <= gacha[i].rate){
                    characterCard.characterCard = RewardCharacter(gacha[i].rarityType);
                    characterCard.updateCard();
                    bool hasNoDuplicates = checkDuplicate(characterCardsGachaed,RewardCharacter(gacha[i].rarityType).id);   
                    if (hasNoDuplicates)
                    {
                        SaveToJson(RewardCharacter(gacha[i].rarityType).id,characterCardsGachaed,characterDataPath);
                    }
                    return;
                }
            }
        }else if(rndType == 2)
        {
            weaponCard = InstatiateCard(weaponCardGO,parent).GetComponent<WeaponCard>();

            for(int i=0; i<gacha.Length; i++){
                if(rnd <= gacha[i].rate){
                    weaponCard.weaponCard = RewardWeapon(gacha[i].rarityType);
                    weaponCard.updateCard();
                    SaveToJson(RewardWeapon(gacha[i].rarityType).id,weaponCardsGachaed,weaponDataPath);
                    return;
                }
            }
        }
    }
    public void InventoryWeapon(){
        for(int i= 0; i<weaponCardsGachaed.list.Count;i++){
            int index = i;
            weaponCards[i] = InstatiateCard(weaponCardGO,history).GetComponent<WeaponCard>();
            weaponCards[i].weaponCard = FindWeaponDataById(weaponCardsGachaed.list[i]);
            weaponCards[i].updateCard();
            CharacterManager.Instance.weaponCardsOwned.Add(weaponCards[i]);
            Button buttonComponent = weaponCards[i].GetComponent<Button>();
            
            if (buttonComponent == null)
            {
                buttonComponent = weaponCards[i].gameObject.AddComponent<Button>();
            }
            //Debug.Log(weaponCards[0]);
            // Add a click listener
            buttonComponent.onClick.AddListener(delegate{CharacterManager.Instance.SetSelectedWeapon(weaponCards[index]);});
        }
    }
    public GameObject InstatiateCard(GameObject cardGO ,Transform pos){
        GameObject cardgameObject = Instantiate(cardGO,pos.position,Quaternion.identity) as GameObject;
        cardgameObject.transform.SetParent(pos);
        cardgameObject.transform.localScale = Vector3.one;
        return cardgameObject;
    }
    public void SingleGacha(){
        if(parent.childCount >0){
            ClearGachaResult(parent);
        }
        Gacha();
        LoadWeaponDataFromJson();
        InventoryWeapon();
    }
    public void MultiGacha(int pulls){
        if(parent.childCount >0){
            ClearGachaResult(parent);
        }
        for (int i= 0; i<pulls; i++){
            Gacha();
        }
        LoadWeaponDataFromJson();
        InventoryWeapon();
    }
    public int Rates(RarityType rarity){
        GachaRate gr = Array.Find(gacha, rt => rt.rarityType == rarity);
        if(gr!= null){
            return gr.rate;
        }
        else
        {
            return 0;
        }
    }

    CharacterData RewardCharacter(RarityType rarity){
        CharacterData[] _characterReward = Array.FindAll(characterReward, RarityType => RarityType.rarity == rarity);

        int rnd = UnityEngine.Random.Range(0,_characterReward.Length);
        return _characterReward[rnd];
    }

    WeaponData RewardWeapon(RarityType rarity){
        WeaponData[] _weaponReward = Array.FindAll(weaponReward, RarityType => RarityType.rarity == rarity);

        int rnd = UnityEngine.Random.Range(0,_weaponReward.Length);
        return _weaponReward[rnd];
    }

    private void ClearGachaResult(Transform parent){
        foreach (Transform child in parent)
        {
            Destroy(child.gameObject);
        }
    }

    public void SaveToJson(int idWeapon ,SerializableList<int> serializableList , string path){
        serializableList.list.Add(idWeapon);

        string jsonData = JsonUtility.ToJson(serializableList);
        File.WriteAllText(path, jsonData);
    }
    
    public void LoadWeaponDataFromJson()
    {
        if (File.Exists(weaponDataPath))
        {
            string jsonData = File.ReadAllText(weaponDataPath);
            
            SerializableList<int> loadedWeaponData = JsonUtility.FromJson<SerializableList<int>>(jsonData);

            if (loadedWeaponData != null)
            {
                // Clear existing data
                weaponCardsGachaed.list.Clear();
                // Use a for loop to add each element to the list
                for (int i = 0; i < loadedWeaponData.list.Count; i++)
                {
                    weaponCardsGachaed.list.Add(loadedWeaponData.list[i]);
                }
            }
            else
            {
                Debug.LogError("Failed to load weapon data from JSON.");
            }
        }
        else
        {
            Debug.LogError("Weapon data JSON file not found: " + weaponDataPath);
        }
    }

    public WeaponData FindWeaponDataById(int id)
    {
        if (weaponReward != null)
        {
            // Iterate through the array to find the WeaponData with the specified ID
            for (int i = 0; i < weaponReward.Length; i++)
            {
                if (weaponReward[i].id == id)
                {
                    // Found the WeaponData with the specified ID
                    return weaponReward[i];
                }
            }

            // Log an error if the WeaponData with the specified ID is not found
            Debug.LogError("WeaponData with ID " + id + " not found.");
        }
        else
        {
            Debug.LogError("WeaponData array is not initialized.");
        }

        // Return null if the WeaponData with the specified ID is not found
        return null;
    }
    private bool checkDuplicate(SerializableList<int> serializableList,int id){
        for(int i= 0 ; i<serializableList.list.Count ; i++){
            if(serializableList.list[i] == id){
                return false;
            }
        }
        return true;
    }


}
[System.Serializable]
public class SerializableList<T> {
    public List<T> list;
}

// [CustomEditor(typeof(GachaManager))]
// public class GachaEditor : Editor
// {
//     public int Common, Rare, Epic;
//     public override void OnInspectorGUI()
//     {
//         base.OnInspectorGUI();
//         EditorGUILayout.Space();

//         GachaManager gm = (GachaManager)target;

//         Common = EditorGUILayout.IntField("Common",(gm.Rates(RarityType.Common) - gm.Rates(RarityType.Rare)));
//         Rare = EditorGUILayout.IntField("Rare",(gm.Rates(RarityType.Rare) - gm.Rates(RarityType.Epic)));
//         Epic = EditorGUILayout.IntField("Epic",gm.Rates(RarityType.Epic));

//     }
// }
