using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using UnityEngine.SceneManagement;
using Unity.VisualScripting;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager Instance;
    public List<CharacterCard> activeCharCard = new List<CharacterCard>();
    public List<WeaponCard> activeWeaponCards = new List<WeaponCard>();
    public List<CharacterCard> allCharCard = new List<CharacterCard>();
    public List<CharacterData> allCharData = new List<CharacterData>();
    public List<WeaponCard> weaponCardsOwned = new List<WeaponCard>();
    private int tempIntCharacter;
    private int tempIntWeapon;
    public GameObject characterPanel;
    public GameObject weaponPanel;
    public GameObject formationPanel;
    public GameObject gameplayPanel;
    [SerializeField] private string characterDataPath;
    void Start()
    {
        characterDataPath = Path.Combine(Application.persistentDataPath, "characterData.json");
        GetCardDataAndLock();
        
    }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void GetCardDataAndLock(){
        for(int i= 0 ; i<allCharCard.Count ; i++){
            allCharCard[i].characterCard = allCharData[i];
            allCharCard[i].lockGO.SetActive(true);
            allCharCard[i].updateCard();
        }
    }
    public void SetTempInt(int _tempInt){
        tempIntCharacter = _tempInt;
        LoadCharacterDataFromJson(characterDataPath);
    }
    public void SetTempIntWeap(int _tempInt){
        tempIntWeapon = _tempInt;
    }
    public void SetSelectedCharacter(CharacterCard characterCard)
    {
        activeCharCard[tempIntCharacter].characterCard = characterCard.characterCard;
        activeCharCard[tempIntCharacter].updateCard();
        characterPanel.SetActive(false);
    }
    public void SetSelectedWeapon(WeaponCard weaponCard){
        activeWeaponCards[tempIntWeapon].weaponCard = weaponCard.weaponCard;
        activeWeaponCards[tempIntWeapon].updateCard();
        weaponPanel.SetActive(false);
    }


    public void LoadCharacterDataFromJson(string path)
    {
        if (File.Exists(path))
        {
            string jsonData = File.ReadAllText(path);
            
            SerializableList<int> loadedData = JsonUtility.FromJson<SerializableList<int>>(jsonData);

            if (loadedData != null)
            {
                foreach (CharacterCard charCard in allCharCard)
                {
                    if (loadedData.list.Contains(charCard.characterCard.id))
                    {
                        // Match found, set the GameObject inactive
                        charCard.lockGO.SetActive(false);
                        Button buttonComponent = charCard.GetComponent<Button>();

                        if (buttonComponent == null)
                        {
                            buttonComponent = charCard.gameObject.AddComponent<Button>();
                        }
                        // Add a click listener
                        buttonComponent.onClick.AddListener(delegate{SetSelectedCharacter(charCard);});
                    }
                }

            }
        }
    }
    public void Play(){
        formationPanel.SetActive(false);
        GameManager.Instance.InstantiateCharacter();
        GameManager.Instance.InstantiateWeapon();
        GameManager.Instance.SetActiveCharacterFromData();
        GameManager.Instance.SpawnCharacter();
        GameManager.Instance.SetWeaponToCharacter();
        GameManager.Instance.SpawnEnemies();
        gameplayPanel.SetActive(true);
    }


}


