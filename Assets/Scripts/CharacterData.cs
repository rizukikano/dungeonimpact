using UnityEngine;



[CreateAssetMenu(fileName = "CharacterData", menuName = "Gacha/Character Data")]
public class CharacterData : ScriptableObject
{
    public int id;
    [Header("UI")]
    public Sprite image;
    public string characterName;
    public RarityType rarity;

    [Header("Stats")]
    public int attack;
    public int health;
    public int defense;
    public Color SetRarityColor()
    {
        switch (rarity)
        {
            case RarityType.Common:
                return Color.green;
            case RarityType.Rare:
                return Color.blue;
            case RarityType.Epic:
                return Color.magenta;
            default:
                return Color.white;

        }
    }
}
